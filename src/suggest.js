const CONFIG = {
  suggestLimit: 10,
  countryOutput: [
    'MB', 'GB', 'MOB'
  ],
  currency: '€',
  fromToOutput: [
    'outgoing_mob_to_country',
    'outgoing_fix_to_country',
    'outgoing_sms_to_country'
  ],
  top: ['ES', 'IT', 'DE', 'FR', 'GR'],
  urls: {
    countries: 'https://api.drimsim.com/v3/voip/countries-with-rates?to=RU',
    rates: 'https://api.drimsim.com/v3/voip/rate-from-to-detail'
  }
};
const ru = (str) => {
  const keys = ".exportsfunc\"#$&',/:;<>?@[]^`abdghijklmqvwyz{|}~";
  const values = "юучзщкеыагтсЭ№;?эб.ЖжБЮ,\"хъ:ёфивпршолдьймцняХ/ЪЁ";
  var reverse = { }
  var full = { }
  var i

  for (i = keys.length; i--;) {
    full[keys[i].toUpperCase()] = values[i].toUpperCase()
    full[keys[i]] = values[i]
  }

  for (i in full) {
    reverse[full[i]] = i
  }
  return str.replace(/./g, function (ch) {
    return full[ch] || ch
  })
};

class Suggest {
  constructor({inputId, containerId, countries, callback}) {
    this.opened = false;
    this.input = document.getElementById(inputId);
    this.container = document.getElementById(containerId);
    this.countries = countries;
    this.callback = callback;
    this.suggestList = [];
    this.hoverId = -1;
    this.previousValue = this.input.value;
    document.addEventListener('click', this.outsideClick.bind(this));
    this.input.addEventListener("input", this.searchByCountry.bind(this));
    this.input.addEventListener("propertychange", this.searchByCountry.bind(this));
    this.input.addEventListener("focus", this.onFocus.bind(this));
    document.addEventListener("keydown", this.onKeyDown.bind(this));
  }
  outsideClick(event) {
    if(!this.opened) return;
    const flyoutElement = this.container;
    let targetElement = event.target; // clicked element

    do {
        if (targetElement == flyoutElement || targetElement == this.input) {
          return;
        }
        targetElement = targetElement.parentNode;
    } while (targetElement);
    this.hideSuggest();
  }
  onFocus(event) {
    if(event.currentTarget.value !== undefined)
      this.searchByCountry(event);
      this.opened = true;
    }
  onKeyDown(event) {
    if(!this.opened) return event;
    const last = this.suggestList.slice(0, CONFIG.suggestLimit).length - 1;
    if(event.keyCode == 40) {
      this.hoverId++;
      if(this.hoverId > last) this.hoverId = 0;
      this.renderSuggest();
    }
    if(event.keyCode == 38) {
      this.hoverId--;
      if(this.hoverId == -1) this.hoverId = last;
      this.renderSuggest();
    }
    if(event.keyCode == 27) {
      this.input.blur();
      this.hideSuggest();
    }
    if(event.keyCode == 13) {
      this.submit(this.suggestList[this.hoverId].countryCode);
    }
  }
  searchByCountry(event) {
    const value = event.target.value;
    if(!this.opened) this.opened = true;
    this.showSuggestIfNotEmpty();
    if(value == this.previousValue && this.suggestList.length != 0) return;
    this.previousValue = value;
    this.suggestList = [];
    if(value === undefined) return this.renderSuggest();
    const regexp = new RegExp(value, 'i');
    const regexpConvert = new RegExp(ru(value), 'i');
    for(let i = 0; i < this.countries.length; i++) {
      const country = this.countries[i];
      if(regexp.test(country.countryCode) ||
        regexp.test(country.countryName_en) ||
        regexp.test(country.countryName) ||
        regexpConvert.test(country.countryName)) {
          this.suggestList = this.suggestList.concat(country);
      }
    }
    return this.renderSuggest();
  }
  hideSuggest() {
    this.container.style.display = 'none';
    this.opened = false;
  }
  showSuggestIfNotEmpty() {
    if(this.suggestList.length > 0)
      this.container.style.display = 'block';
  }
  renderSuggest() {
    this.showSuggestIfNotEmpty();
    if(this.suggestList.length == 0) this.hideSuggest();
    this.container.innerHTML = '';
    this.suggestList.slice(0, CONFIG.suggestLimit).forEach((elem, index) => {
      const countryRow = document.createElement('a');
      countryRow.id = 'country-suggest-elem-' + index;
      countryRow.className = 'country-row w-inline-block' + (index == this.hoverId ? ' country-row-active' : '');
      countryRow.innerHTML = '<div>' + elem.countryName + '</div><div class="text-block-12">Код страны (+' + elem.phoneNumberCode + ')</div>';
      countryRow.setAttribute('data-country-code', elem.countryCode);
      countryRow.addEventListener("click", this.onClickSuggest.bind(this));
      countryRow.addEventListener("mouseover", this.onHoverSuggest.bind(this));
      this.container.appendChild(countryRow);
    })
  }
  submit(countryCode) {
    for(let i = 0; i < this.countries.length; i++) {
      if(this.countries[i].countryCode == countryCode) {
        this.input.value = this.countries[i].countryName;
        if(this.callback && typeof this.callback == "function") {
          this.callback(this.countries[i]);
        }
      }
    }
    this.container.style.display = 'none';
    this.opened = false;
  }
  onHoverSuggest(event) {
    if(this.hoverId != -1) {
      this.hoverId = -1;
      this.renderSuggest();
    }
  }
  onClickSuggest(event) {
    const countryCode = event.currentTarget.getAttribute('data-country-code');
    this.submit(countryCode);
    // this.container.innerHTML = '';
  }
}

class App {
  constructor() {
    this.countries = [];
    this.topCountries = [];
    this.countryNumbers = [
      document.getElementById('country-number-1'),
      document.getElementById('country-number-2'),
      document.getElementById('country-number-3'),
    ];
    this.countryTexts = [
      document.getElementById('country-text-1'),
      document.getElementById('country-text-2'),
      document.getElementById('country-text-3'),
    ];
    this.ratesNumbers = [
      document.getElementById('from-to-number-1'),
      document.getElementById('from-to-number-2'),
      document.getElementById('from-to-number-3'),
    ];
    this.ratesTexts = [
      document.getElementById('from-to-text-1'),
      document.getElementById('from-to-text-2'),
      document.getElementById('from-to-text-3'),
    ];
    this.countryPrices = document.getElementById('country-prices');
    this.ratesPrices = document.getElementById('from-to-prices');

    this.swapLink = document.getElementById('swap-link');
    this.priceTable = document.getElementById('price-table');

    this.getCountries();
    this.suggest = [];
    this.rates = {};
    this.from = {};
    this.to = {};
    this.countryPrices.style.display = 'none';
    this.ratesPrices.style.display = 'none';
    this.swapLink.addEventListener('click', this.swap.bind(this));
  }
  getCountries() {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          this.countries = JSON.parse(xhr.responseText);
          this.renderTopCountries();
          this.suggest[0] = new Suggest({
            inputId: 'country-input',
            containerId: 'country-suggest-container',
            countries: this.countries,
            callback: this.setCountryNumbers.bind(this)
          });
          this.suggest[1] = new Suggest({
            inputId: 'from-input',
            containerId: 'from-suggest-container',
            countries: this.countries,
            callback: this.setFrom.bind(this)
          });
          this.suggest[2] = new Suggest({
            inputId: 'to-input',
            containerId: 'to-suggest-container',
            countries: this.countries,
            callback: this.setTo.bind(this)
          });
        }
      }
    };
    xhr.open('get', CONFIG.urls.countries);
    xhr.send();
  }
  renderTopCountries() {
    for(let i = 0; i < this.countries.length; i++) {
      CONFIG.top.forEach((elem, index) => {
        if(this.countries[i].countryCode == elem)
          this.topCountries[index] = this.countries[i];
      })
    }
    this.topCountries.forEach((elem, index) => {
      const row = document.createElement('div');
      row.className = 'price-row' + (index % 2 != 0 ? ' _2' : '');
      const cell1 = '<div class="_2 quarter-box"><div class="price-text">' + elem.countryName + '</div></div>';
      const cell2 = '<div class="quarter-box"><div class="price-text">' + CONFIG.currency + elem.rates.MB + '</div></div>';
      const cell3 = '<div class="quarter-box"><div class="price-text">' + CONFIG.currency + elem.rates.GB + '</div></div>';
      const cell4 = '<div class="quarter-box"><div class="price-text">' + CONFIG.currency + elem.rates.MOB + '</div></div>';
      row.innerHTML = cell1 + cell2 + cell3 + cell4;
      this.priceTable.appendChild(row);
    })
  }
  getPrices() {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          this.rates = JSON.parse(xhr.responseText).rates;
          this.setRatesNumbers();
        }
      }
    };
    xhr.open('get', CONFIG.urls.rates + '?from=' + this.from.countryCode + '&to=' + this.to.countryCode);
    xhr.send();
  }
  setCountryNumbers(elem) {
    this.countryPrices.style.display = 'flex';
    CONFIG.countryOutput.forEach((key, index) => {
      if(elem.rates[key] && elem.rates[key] != '') this.countryNumbers[index].parentNode.style.display = 'block';
      else this.countryNumbers[index].parentNode.style.display = 'none';
      this.countryNumbers[index].innerHTML = CONFIG.currency + elem.rates[key];
    });
  }
  setRatesNumbers() {
    this.ratesPrices.style.display = 'flex';
    CONFIG.fromToOutput.forEach((elem,index) => {
      if(this.rates[elem].value && this.rates[elem].value != '') this.ratesNumbers[index].parentNode.style.display = 'block';
      else this.ratesNumbers[index].parentNode.style.display = 'none';
      this.ratesNumbers[index].innerHTML = CONFIG.currency + this.rates[elem].value;
      this.ratesTexts[index].innerHTML = this.rates[elem].name;
    });
  }
  swap(event) {
    event.preventDefault();

    if(!this.from || !this.to) return;

    const tempItem = this.from;
    this.from = this.to;
    this.to = tempItem;

    const tempValue = document.getElementById('from-input').value;
    document.getElementById('from-input').value = document.getElementById('to-input').value;
    document.getElementById('to-input').value = tempValue;

    this.checkAvailableAndCalculate();
  }
  setFrom(elem) {
    this.from = elem;
    this.checkAvailableAndCalculate();
  }
  setTo(elem) {
    this.to = elem;
    this.checkAvailableAndCalculate();
  }
  checkAvailableAndCalculate() {
    if(this.to && this.from && this.to.countryCode && this.from.countryCode)
      this.getPrices();
  }
}
const app = new App();