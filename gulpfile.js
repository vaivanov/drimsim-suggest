const gulp = require('gulp');
const babel = require('gulp-babel');

gulp.task('js', () => {
  return gulp.src('./src/*.js')
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('./build'));
});


gulp.task('default', ['js']);